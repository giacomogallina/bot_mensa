# `mensa.py`
Contiene una piccola e leggera libreria per fare prenotazioni a mensa,
che non usa Selenium (adatta ad esempio per un Raspberry Pi, dove col cavolo che
si riesce a far girare Firefox)

# `bot.py`
Contiene un esempio minimo di bot che, utilizzando `mensa.py`, prenota
ogni giorno pranzo e cena alla stessa ora, e, se qualcosa va storto, manda una
mail per avvisare del problema. Non dovrebbe essere difficile renderlo
un po' più potente

Per usarlo basta eseguirlo, ed inserire mail e password
```
~$ ./bot.py
email: nome.cognome@sns.it
Password: ************
```
per evitare di tenere aperto un terminale tutto il tempo, può essere comodo
usare GNU screen

# `spazi.pem`
Non ho capito bene come funzioni la cosa, ma a python non piace il certificato
TLS di spazi. Da quel che ho capito è perchè è firmato da qualcuno di cui non si
fida direttamente, e quindi dovrebbe andare a chiedere a questo qualcuno il suo
certificato (cosa che un browser sembra fare). Invece, python si arrende subito
e rompe le scatole. Quindi, seguendo quanto spiegato [qui](https://stackoverflow.com/questions/46604114/python-requests-ssl-error-certificate-verify-failed#comment107645776_46604917),
ho scaricato la fantomatica "complete certificate chain" di spazi e l'ho
messa in `spazi.pem`

