#!/usr/bin/python3

# esempio minimo di bot per prenotare a mensa
# può essere un punto di partenza ragionevole per un bot un po' più sofisticato,
# ad esempio che prenoti a orari diversi in base al giorno della settimana

from mensa import User
import datetime
import schedule
import time
import logging
import argparse
import sys

parser = argparse.ArgumentParser(description="Prenota a mensa all'orario che piace a me")
parser.add_argument("-n", help = "il numero di account da gestire (default 1)", type = int, default = 1)
args = parser.parse_args()

logger = logging.getLogger(__name__)

# crea gli utente, chiedendo da stdin le mail e le password
users = []
for i in range(args.n):
    while True:
        try:
            users.append(User())
            print("ok!")
            break
        except KeyboardInterrupt:
            print("\nquitting")
            sys.exit()
        except:
            print("login failed, retry")


# prenota sempre alla stessa ora
def book_for_today():
    date = str(datetime.date.today())
    logger.info("booking...")
    for user in users:
        for time in ["12:30", "19:30"]:
            try:
                # linea __.
                #         |
                user.book(2, date, time)
            except Exception as err:
                logger.exception("booking failed:")
                try:
                    user.send_email(user.email,
                            f"Non sono riuscito a prenotare alle {time}",
                            f"Ho incontrato un {type(err)}:\n{err}\n\nsorry,\nBot"
                    )
                except:
                    logger.exception("even the mail failed:")

schedule.every().day.at("01:00").do(book_for_today)

while True:
    schedule.run_pending()
    time.sleep(10)
