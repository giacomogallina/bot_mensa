# piccola libreria per fare prenotazioni a mensa, senza stare a usare Selenium
# che se no il povero Raspberry Pi muore

import time
import requests
import smtplib
import email
import string
import random
import getpass
from bs4 import BeautifulSoup
import json


def check_200(reply, name):
    if reply.status_code >= 400:
        raise Exception(f"{name} returned {reply.status_code}")


# utente dotato di mail e password, può prenotare posti a mensa e mandare email
class User:
    email = ""
    passwd = ""
    cached_sess_id = ""
    cached_sess_id_creation_time = 0

    # chiede da stdin la mail e la password, nel caso in cui non siano passate
    # esplicitamente alla funzione, e prova a fare un login per controllare se
    # sono corrette. In caso contrario, lancia un'eccezione
    def __init__(self, email = "", passwd = ""):
        self.email = email or input("email: ")
        self.passwd = passwd or getpass.getpass()
        self.authenticate()

    
    # restituisce un id casuale di sessione autenticato:
    # da ora in poi (per una decina di minuti) l'id testimonia che l'utente si
    # è loggato con successo, e può essere usato per fare prenotazioni
    def authenticate(self):
        if time.time() - self.cached_sess_id_creation_time > 300:
            chars = string.ascii_lowercase + string.digits
            my_id = ''.join(random.choice(chars) for i in range(26))

            reply = requests.post(
                    "https://spazi.sns.it/index.php",
                    verify="spazi.pem",
                    data = {
                        "email": self.email,
                        "password": self.passwd,
                        "login":"submit"
                    },
                    headers = {
                        "Cookie": "PHPSESSID=" + my_id
                    }
            )
            check_200(reply, "authentication")
            if b"loginError" in reply.content:
                raise Exception("Wrong Credentials")

            self.cached_sess_id = my_id
            self.cached_sess_id_creation_time = time.time()

        return self.cached_sess_id


    # prenota un posto in mensa (capendo dall'orario se è per pranzo o cena), e,
    # se qualcosa va storto, stampa su stdout la risposta ricevuta dal server
    # line dev'essere o 1 o 2
    # date dev'essere una stringa tipo "2021-03-28"
    # time dev'essere una stringa del tipo "08:45"
    # restituisce il referenceNumber della prenotazione, che può essere usato,
    # ad esempio, per annullarla
    # Esempio:
    # ref = user.book(2, "2021-03-28", "13:15")  <- prenota alle 13:15 in linea 2
    def book(self, line, date, time, userId = None):
        hour, minute = map(int, time.split(":"))
        meal = int(hour > 16)

        # crea un nuovo id di sessione lo autentica
        my_id = self.authenticate()

        # .........|.pranzo.|..cena..|
        #  linea 1 |  1278  |  1281  |
        #  linea 2 |  1279  |  1280  |
        resId = [1277 + line, 1282 - line][meal]

        # per prenotare servono l'id utente e qualsiasi cosa sia un csrf_token,
        # quindi li chiediamo al server
        reply = requests.get(
                f"https://spazi.sns.it/reservation.php?rid={resId}&sid={31 + meal}&rd={date}&sd={date} 00:00:00&ed={date} 00:00:00",
                verify = "spazi.pem",
                headers = {
                    "Cookie": "PHPSESSID=" + my_id
                },
        )
        check_200(reply, "getting info")
        soup = BeautifulSoup(reply.content, 'html.parser')
        token = soup.find(id="csrf_token").get("value")
        if userId is None:
            userId = soup.find(id="userName").get("data-userid")

        # la prenotazione vera e propria!
        reply = requests.post(
                "https://spazi.sns.it/ajax/reservation_save.php",
                verify="spazi.pem",
                data = {
                    "userId": userId,
                    "beginDate": date,
                    "beginPeriod": f"{hour:02}:{minute:02}:00",
                    "endDate": date,
                    "endPeriod": f"{hour + (minute + 15) // 60 :02}:{(minute + 15) % 60 :02}:00",
                    "repeatOptions": "none",
                    "scheduleId": 31 + meal,
                    "resourceId": resId,
                    "reservationAction": "create",
                    "seriesUpdateScope": "full",
                    "CSRF_TOKEN": token
                },
                headers = {
                    "Cookie": "PHPSESSID=" + my_id
                }
        )
        check_200(reply, "booking")

        # il server ci risponde con l'html del popup che compare quando si
        # prenota, e, se qualcosa è andato storto, lo stampiamo su stdout e
        # tiriamo un'eccezione
        soup = BeautifulSoup(reply.content, 'html.parser')
        if "success" not in str(reply.content):
            raise Exception("booking failed: \n\n" + soup.prettify())
        else:
            # restituiamo il referenceNumber della prenotazione, in modo ad
            # esempio da poterla cancellare
            return soup.find(id="reference-number").text.strip().split()[-1]


    
    # cancella la prenotazione con referenceNumber ref
    # Esempio:
    # ref = user.book(2, "2021-03-28", "13:15")  <- prenota alle 13:15 in linea 2
    # user.delete_reservation(ref)               <- cancella la prenotazione
    def delete_reservation(self, ref):
        my_id = self.authenticate()
        reply = requests.get(
                f"https://spazi.sns.it/reservation.php?rn={ref}",
                verify = "spazi.pem",
                headers = {
                    "Cookie": "PHPSESSID=" + my_id
                },
        )
        check_200(reply, "getting info")
        soup = BeautifulSoup(reply.content, 'html.parser')
        token = soup.find(id="csrf_token").get("value")
        resId = soup.find(id="primaryResourceId").get("value")
        reservationId = soup.find(attrs = {"name":"reservationId"}).get("value")
        if soup.find(id="userName"):
            bp = soup.find(id="BeginPeriod").find(attrs = {"selected": "selected"}).get("value")
        else:
            bp = soup.find(id = "BeginPeriod").get("value")


        reply = requests.post(
                "https://spazi.sns.it/ajax/reservation_delete.php",
                verify="spazi.pem",
                data = {
                    "scheduleId": 31 + int(int(bp.split(":")[0]) > 16),
                    "resourceId": resId,
                    "reservationId": reservationId,
                    "referenceNumber": ref,
                    "reservationAction": "update",
                    "seriesUpdateScope": "full",
                    "CSRF_TOKEN": token
                },
                headers = {
                    "Cookie": "PHPSESSID=" + my_id
                }
        )
        check_200(reply, "deleting reservation")
        if "success" not in str(reply.content):
            soup = BeautifulSoup(reply.content, 'html.parser')
            raise Exception("deleting reservation failed: \n\n" + soup.prettify())



    def send_email(self, target, subject, text):
        msg = email.message.EmailMessage()
        msg["Subject"] = subject
        msg["From"] = self.email
        msg["To"] = target
        msg.set_content(text)

        server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server.ehlo()
        server.login(self.email, self.passwd)

        server.send_message(msg)


    # restituisce le prenotazioni già presenti per un certo pasto in una certa data
    # date dev'essere una stringa del tipo "2021-03-28"
    # meal dev'essere un intero, 0 per il pranzo e 1 per la cena
    def get_reservations(self, date, meal):
        my_id = self.authenticate()

        reply = requests.post(
                "https://spazi.sns.it/schedule.php?dr=reservations",
                verify = "spazi.pem",
                headers = {
                    "Cookie": "PHPSESSID=" + my_id
                },
                data = {
                    "beginDate": date,
                    "endDate": date,
                    "scheduleId": str(31 + meal),
                }
        )
        check_200(reply, "getting reservations")

        return json.loads(reply.content)

